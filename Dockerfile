FROM python:3.6.3-alpine3.6

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/

RUN apk add --no-cache --virtual .build-deps \ 
    build-base \
    ncurses-dev \
    bash \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del .build-deps

COPY . /usr/src/app/

ENV FLASK_APP=/usr/src/app/run.py
ENV FLAK_DEBUG=1

ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
