import pytest

from hugzreviews import create_app

pytest_plugins = ['flask']

@pytest.fixture
def app(request):
    app = create_app()
    app.testing = True
    with app.app_context():
        yield app
