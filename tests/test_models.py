import pytest

from mongoengine.errors import ValidationError
from hugzreviews.models import Review, ReviewSchema

def test_review_negative():
    review = Review(thing="dolan trump", rating=-1)
    review.save()

def test_review_positive():
    review = Review(thing="hialry clintno", rating=11)
    review.save()

def test_review_invalid_rating():
    review = Review(thing="no", rating=5)
    with pytest.raises(ValidationError):
        review.save()

def test_review_with_comments():
    review = Review(thing="Surface Ergonomic Keyboard", rating=-1, comments="it looks like carpet")
    review.save()
