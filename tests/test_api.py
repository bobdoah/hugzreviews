from flask import url_for

def test_api_get_reviews_empty(client, app):
    res = client.get(url_for('api.get_reviews'))
    assert res.status_code == 200
    assert res.json == []

