from flask_mongoengine import MongoEngine
import marshmallow_mongoengine as ma

db = MongoEngine()

class Review(db.Document):
    thing = db.StringField(required=True)
    comments = db.StringField()
    rating = db.IntField(required=True, choices=(-1, 11))


class ReviewSchema(ma.ModelSchema):
    class Meta:
        model = Review
