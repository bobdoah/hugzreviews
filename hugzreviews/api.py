from flask import current_app, Blueprint, jsonify, request

from .models import Review, ReviewSchema

review_schema = ReviewSchema()
reviews_schema = ReviewSchema(many=True)

api = Blueprint("api", __name__, url_prefix="/api")

@api.route('/reviews/')
def get_reviews():
    reviews = Review.objects.all()
    data, err = reviews_schema.dump(reviews)
    if err:
        return jsonify(err), 500
    return jsonify(data)

@api.route('/review/')
def get_review(review_id):
    try:
        review = Review.objects.get(review_id)
        data, err = review_schema.dump(review)
        if err:
            return jsonify(err), 500
        return jsonify(data)
    except Review.DoesNotExist:
        return jsonify("Review id {} not found".format(review_id)), 404
    except Review.MultipleObjectsReturned:
        return jsonify("Multiple objects returned for id {}".format(review_id)), 500

@api.route('/review/', methods=['POST'])
def post_review():
    json_data = request.get_json()
    review, err = review_schema.load(json_data)
    if err:
        return jsonify(err), 422
    review.save()
    return jsonify(review)
