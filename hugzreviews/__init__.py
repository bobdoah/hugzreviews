from flask import Flask 

def create_app():
    app = Flask(__name__)
    app.config['MONGODB_HOST'] = 'mongomock://localhost'
    app.config['MONGODB_DB'] = 'hugzreviews'
    app.config['MONGODB_IS_MOCK'] = True

    from .models import db
    db.init_app(app)

    from .api import api
    app.register_blueprint(api)
    
    return app
